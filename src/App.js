import React from 'react';
import Routes from './routes/route';
import {Provider} from "react-redux"
import { BrowserRouter } from "react-router-dom";
import store from "./store";
import './App.css';

function App() {
  return (
    <Provider store={store}>
    <BrowserRouter>
    <Routes />
  </BrowserRouter>
  </Provider>
  );
}

export default App;
