import React, {useEffect} from 'react';
// import { useSelector } from 'react-redux';
import {getDetail} from '../store/action/destination';
import {  useSelector, useDispatch } from "react-redux"
import '../assets/styles/Homepage.scss'

function Homepage()  {
    const dispatch = useDispatch()
    const detail = useSelector((state) => state.Search);
    console.log(detail, 'detail')
    // const details = detail.map(item => {
    //     return (
    //  <div key = {item.Title} className='poster'>
    //         <a href='/detail'>
    //         <img alt='banner' src={item.Poster}/>
    //         </a>
    //     </div>
    //     )
    //  })
    
    
    useEffect(() => {
        dispatch(getDetail()
        );
    }
    )

        return (
            <div className='wrapper'>
                <div className='header'>
                    <img src={require('../assets/images/images.jpeg')}></img>
                </div>
                <div className='content-wrapper'>                    
                {detail.map(item => {
        return (
     <div key = {item.Title} className='poster'>
            <a href='/detail'>
            <img alt='banner' src={item.Poster}/>
            </a>
        </div>
        )
     })  }
                </div>
                
            </div>
        )
}


export default Homepage;