import React, { useEffect, useState } from "react";
import { useSelector } from 'react-redux';
import { useParams } from "react-router-dom";
import {getDetail} from '../store/action/destination';

function Detail() {
    const title = useSelector((state) => state.Search.titles);
    const imdbID = useSelector((state) => state.Search.imdbIDs);
    useEffect(() => {
        dispatch(getDetail()
        );
    }
    );

return (
    <div>
    <div className='header'>
        <h3>Home  </h3>
    </div>
    <div className='wrapper'>
        <div className='banner'>
            <img src={Search.poster} alt='banner'></img>
        </div>
        <div className='information'>
            <p>1 in stock</p>
            <p>{Search.imdbID}</p>
            <h2>{Search.title}</h2>
            <h5>Paramaters</h5>
            <h5>Description: </h5>
            <p>Lorem dipsum is lorem things</p>
        </div>
    </div>
    </div>
)
}

export default Detail;