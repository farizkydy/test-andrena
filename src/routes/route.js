import React from 'react';
import Homepage from '../components/Homepage';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Detail from '../layout/Detail';

const Routes = () => {
    return (
        <Router>
        <Switch>
            <Route path='/' component={Homepage} exact/>
            <Route path='/detail' component={Detail} exact/>
        </Switch>
        </Router>
    )
}
export default Routes;