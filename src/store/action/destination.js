import GET_DETAIL from './types'

const baseUrl = 'https://www.omdbapi.com/?apiKey=b445ca0b&s=avenger'

export const getDetail = () => async dispatch => {
    try {
        const getDetailRes = await fetch (
            `${baseUrl}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'Application/JSON',
                accept: 'application/JSON'
            }
        }
        ); 
        const dataDetail = await getDetailRes.JSON();
            console.log('dataDetail', dataDetail)
        dispatch({
            type: 'GET_DETAIL',
            payload: dataDetail.data[0]
        })
       
    }catch (eror) {
        console.log(eror)
    }

}