import React from 'react';
import {GET_DETAIL} from '../action/types';

const initialState = {
    Search: [],
    // Title: '',
    // Year: '',
    // imdbID: '',
    // Type: '',
    // Poster: ''
}

const detailReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DETAIL:
        return {...state, Search: action.payload};
        default:
            return state;
    }
}
export default detailReducer