import {combineReducers} from 'redux';
import Search from './destination';

const rootReducers = combineReducers ({
    Search
});

export default rootReducers;